import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SubscriptionSearch {

	JFrame frame;
	private JTextField textField;
	private JTextField textField_1;

	private Database dbase = null;
	private Subscription sc=null;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public SubscriptionSearch(String s,Database d,Subscription sub) {
		super();
		dbase=d;
		initialize();
		sc=sub;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 538, 465);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setForeground(Color.BLACK);
		panel.setBorder(BorderFactory.createTitledBorder( "Search"));
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(46, 13, 435, 99);
		frame.getContentPane().add(panel);
		
		JLabel label = new JLabel("SearchByID:                 ");
		label.setBounds(6, 23, 233, 26);
		panel.add(label);
		
		textField = new JTextField(10);
		textField.setBounds(6, 49, 233, 26);
		panel.add(textField);
		
		JButton button = new JButton("Search");
		button.setBounds(249, 49, 113, 27);
		panel.add(button);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setForeground(Color.BLACK);
		panel_1.setBorder(BorderFactory.createTitledBorder( "SearchByID"));
		panel_1.setBackground(Color.LIGHT_GRAY);
		panel_1.setBounds(46, 125, 435, 261);
		frame.getContentPane().add(panel_1);
		
		JLabel label_1 = new JLabel("ID:                 ");
		label_1.setBounds(14, 23, 233, 26);
		panel_1.add(label_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(115, 24, 288, 24);
		panel_1.add(textField_1);
		
		JButton button_1 = new JButton("Modify");
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button_1.setBounds(6, 206, 175, 27);
		panel_1.add(button_1);
		
		JButton button_2 = new JButton("Cancel");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Code to open the main menu			
				MainMenu main = new MainMenu();
				main.frame.setVisible(true);
				frame.setVisible(false); 
				frame.dispose(); 
			}
		});
		button_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button_2.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		button_2.setBounds(223, 206, 182, 27);
		panel_1.add(button_2);
		
		JCheckBox checkBox = new JCheckBox("Irish Independent");
		checkBox.setBounds(6, 80, 175, 25);
		panel_1.add(checkBox);
		
		JCheckBox checkBox_1 = new JCheckBox("Irish Mirror");
		checkBox_1.setBounds(223, 80, 182, 25);
		panel_1.add(checkBox_1);
		
		JCheckBox checkBox_2 = new JCheckBox("Longford Leader");
		checkBox_2.setBounds(6, 136, 175, 25);
		panel_1.add(checkBox_2);
		
		JCheckBox checkBox_3 = new JCheckBox("Westmeath Topic");
		checkBox_3.setBounds(223, 136, 182, 25);
		panel_1.add(checkBox_3);
	}
}
